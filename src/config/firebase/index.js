// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyAEodig4ABqj50ad5DqMtuj4hA1KMVQFfQ",
  authDomain: "cov-id.firebaseapp.com",
  databaseURL: "https://cov-id.firebaseio.com",
  projectId: "cov-id",
  storageBucket: "cov-id.appspot.com",
  messagingSenderId: "332423586992",
  appId: "1:332423586992:web:1471f6a03710015bcbc775",
  measurementId: "G-78GT4C4CH4"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
