/* eslint-disable jsx-a11y/alt-text */
import React, { Component, Fragment } from "react";
import Button from "../../../component/atom/Button";
import Input from "../../../component/atom/Input";
import login from "../../../assets/img/login.png";
import bg from "../../../assets/img/bg-login.png";
import "./Login.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Login extends Component {
  render() {
    return <Fragment>login</Fragment>;
  }
}

const reduxState = state => ({
  popupProps: state.popup
});

export default connect(reduxState, null)(Login);
