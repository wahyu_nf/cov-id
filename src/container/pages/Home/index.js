import React, { Component, Fragment } from "react";
import Nav from "../../organisme/Navbar";
import "./Home.css";
import bgHome from "../../../assets/img/bg-home.png";
import bgBgHome from "../../../assets/img/bg-bg-home.png";

export default class Home extends Component {
  state = {
    total: {}
  };

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://corona.lmao.ninja/all";

    fetch(proxyurl + url)
      .then(response => response.json())
      .then(contents => {
        console.log(contents);
        this.setState({
          total: contents
        });
      })
      .catch(() =>
        console.log("Can’t access " + url + " response. Blocked by browser?")
      );
  }

  render() {
    return (
      <Fragment>
        <Nav />
        <div className="container-home">
          <div class="content-a">
            <h4>Data Covid-19 Dunia</h4>
            <p>Jumlah Kasus: </p>
            <p className="count">{this.state.total.cases}</p>
            <p>Jumlah yang Maninggal: </p>
            <p className="count">{this.state.total.deaths}</p>
            <p>Jumlah yang Sembuh: </p>
            <p className="count">{this.state.total.recovered}</p>
          </div>
          <div class="img">
            <img src={bgHome} alt="" />
          </div>
        </div>
      </Fragment>
    );
  }
}
