/* eslint-disable jsx-a11y/alt-text */
import React, { Component, Fragment } from "react";
import Button from "../../../component/atom/Button";
import Input from "../../../component/atom/Input";
import registrasi from "../../../assets/img/registrasi.png";
import bg from "../../../assets/img/bg-login.png";
import "./Registrasi.scss";
import firebase from "../../../config/firebase";

export default class Login extends Component {
  state = {
    userName: "",
    email: "",
    password: "",
    conPassword: ""
  };

  hendleChangeText = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  hendleClickSubmit = () => {
    console.log(this.state.userName);
    console.log(this.state.password);
    if (this.state.password === this.state.conPassword) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(res => {
          console.log("success : " + res);
        })
        .catch(function(error) {
          // Handle Errors here.
          console.log(error.code + error.message);
        });
    } else {
      alert("Password Anda tidak sama");
    }
  };

  render() {
    return (
      <Fragment>
        <p>registrasi</p>
      </Fragment>
    );
  }
}
