import React from "react";
import "./App.css";
import Home from "../Home";
import Login from "../Login";
import Registrasi from "../Registrasi";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "../../../config/redux";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/registrasi" component={Registrasi} />
      </Router>
    </Provider>
  );
}

export default App;
