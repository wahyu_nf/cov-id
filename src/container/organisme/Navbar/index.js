/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import "materialize-css/dist/css/materialize.min.css";

export default class Navbar extends Component {
  componentDidMount() {
    let sidenav = document.querySelector(".sidenav");
    M.Sidenav.init(sidenav, {});
  }

  render() {
    return (
      <div className="App">
        <nav>
          <div class="container">
            <div className="nav-wrapper blue darken-1">
              <a href="#" className="left brand-logo">
                cov-id
              </a>
              <a href="#" data-target="mobile" className="sidenav-trigger">
                <i className="material-icons">menu</i>
              </a>
              <ul className="right hide-on-med-and-down">
                <li>
                  <a href="sass.html">Home</a>
                </li>
                <li>
                  <a href="badges.html">Olahraga</a>
                </li>
                <li>
                  <a href="collapsible.html">Makanan</a>
                </li>
                <li>
                  <a href="mobile.html">Info</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <ul className="sidenav" id="mobile">
          <li>
            <a href="sass.html">Home</a>
          </li>
          <li>
            <a href="badges.html">Olahraga</a>
          </li>
          <li>
            <a href="collapsible.html">Makanan</a>
          </li>
          <li>
            <a href="mobile.html">Info</a>
          </li>
        </ul>
      </div>
    );
  }
}
