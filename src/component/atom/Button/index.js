import React from "react";
import "./Button.scss";

const Button = p => {
  if (p.jen === "true") {
    return (
      <getAllByDisplayValue>
        <button className="btn-me" type={p.type} onClick={p.onClick}>
          {p.judul}
        </button>
      </getAllByDisplayValue>
    );
  } else {
    return (
      <div>
        <button className="btn-me-daf" type={p.type}>
          {p.judul}
        </button>
      </div>
    );
  }
};

export default Button;
