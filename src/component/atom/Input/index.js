import React from "react";
import "./Input.scss";

const Input = p => {
  return (
    <div className="container-in">
      <input
        type={p.type}
        name={p.name}
        placeholder={p.placeholder}
        className="input-me"
        onChange={p.onChangeText}
      />
    </div>
  );
};

export default Input;
